#ifndef SHOP_H
#define SHOP_H
#include<QMutex>
#include<QLabel>
#include<memento.h>
#include"shopeventmanager.h"
class Shop
{
private:
    int CountCoints=0;
    ShopEventManager* shopEventManager;
    Shop();
     static Shop *Instanse;
     static QMutex *lock;
public:
    static Shop* GetInstanse();
    void AddCoins(int);
    void UpdataCoinsLabel(QLabel*);

    Memento* Save(QString File);
    void Load(Memento*);

    friend QDataStream& operator<<(QDataStream &out, Shop *shop){
        out<<shop->CountCoints;
        return out;
    }
    friend QDataStream& operator>>(QDataStream &in, Shop *shop){
        in>>shop->CountCoints;
        return in;
    }
    void setCountCoints(int value);
    int getCountCoints() const;
    ShopEventManager *getShopEventManager() const;
};

#endif // SHOP_H
