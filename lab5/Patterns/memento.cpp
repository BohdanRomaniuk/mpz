#include "memento.h"
#include "shop.h"

Memento::Memento(QString File):FileName(File)
{
  QFile file(FileName);
  file.open(QIODevice::WriteOnly);
  QDataStream stream(&file);
  stream << Shop::GetInstanse();
}
void Memento::Load()
{
    QFile file(FileName);
    file.open(QIODevice::ReadOnly);
    QDataStream stream(&file);
    stream >> Shop::GetInstanse();
}
