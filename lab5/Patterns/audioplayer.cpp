#include "audioplayer.h"

AudioPlayer::AudioPlayer()
{
    playlist = new QMediaPlaylist();
    playlist->addMedia(QUrl("https://www.youtube.com/watch?v=PyWofcEB5vs&list=RDPyWofcEB5vs&start_radio=1"));
    playlist->addMedia(QUrl("https://www.youtube.com/watch?v=PyWofcEB5vs&list=RDPyWofcEB5vs&start_radio=1"));
    playlist->addMedia(QUrl("https://www.youtube.com/watch?v=PyWofcEB5vs&list=RDPyWofcEB5vs&start_radio=1"));
    playlist->setCurrentIndex(1);

    player = new QMediaPlayer;
    player->setPlaylist(playlist);
    player->setVolume(Volume);
    state->setContext(this);

}

AudioPlayer::AudioPlayer(States *_state)
{
    playlist = new QMediaPlaylist();
    playlist->addMedia(QUrl::fromLocalFile("D:/Study/2/Term2/mpz/Lab7/Sounds/rammstein-deutschland.mp3"));
    playlist->addMedia(QUrl::fromLocalFile("D:/Study/2/Term2/mpz/Lab7/Sounds/rammstein-ohne_dich.mp3"));
    playlist->addMedia(QUrl::fromLocalFile("D:/Study/2/Term2/mpz/Lab7/Sounds/rammstein-radio.mp3"));
    playlist->setCurrentIndex(CurrSong);

    player = new QMediaPlayer;
    player->setPlaylist(playlist);
    player->setVolume(Volume);
    this->state=_state;
    state->setContext(this);
}

States *AudioPlayer::ChangeState(States *_state)
{
    this->state=_state;
    state->setContext(this);
    return state;
}

void AudioPlayer::clickLock()
{
    state->clickLock();
}

void AudioPlayer::clickNext()
{
    state->clickNext();
}

void AudioPlayer::clickPlay()
{
    state->clickPlay();
}

void AudioPlayer::clickPrevious()
{
    state->clickPrevious();
}

void AudioPlayer::startPlayback()
{
    player->play();
}

void AudioPlayer::pausePlayback()
{
    player->pause();
}

void AudioPlayer::nextSong()
{
     playlist->next();
}

void AudioPlayer::previousSong()
{
    playlist->previous();
}
