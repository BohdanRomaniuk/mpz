#ifndef SHOPCONTROLLFACADE_H
#define SHOPCONTROLLFACADE_H
#include"shop.h"
#include"QMediaPlayer"

class ShopControllFacade
{
protected:
    Shop *shop;
    QMediaPlayer *soundManager;
public:
    ShopControllFacade();
    ShopControllFacade(Shop *shop,QMediaPlayer *soundManager){
        this->shop=shop;
        this->soundManager=soundManager;
    }
    void BuyCoins(int coins,QLabel* label);
};

#endif // SHOPCONTROLLFACADE_H
