#ifndef FLYWEIGHT_H
#define FLYWEIGHT_H
#include"QPixmap"
#include"QMutex"
class Flyweight
{
    QPixmap *BadCardPicture;
    QPixmap *BonusCardPicture;
    Flyweight();
    Flyweight(QPixmap *,QPixmap *);
    static Flyweight* Instanse;
    static QMutex *lock;
public:
    static Flyweight* GetInstanse();

    QPixmap* GetBadCardPicture();
    QPixmap* GetBonusCardPicture();
};

#endif // FLYWEIGHT_H
