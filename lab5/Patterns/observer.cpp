#include "observer.h"
#include"shop.h"
BonusListener::BonusListener()
{

}

void SmallBonusListener::Update(ShopEventManager *sem)
{
    if(sem->getBoughtCoins()>=100&& !sem->getSmallBonusReceived()){
        sem->setSmallBonusReceived(true);
        Shop::GetInstanse()->setCountCoints(Shop::GetInstanse()->getCountCoints()+20);
    }
}

void BigBonusListener::Update(ShopEventManager *sem)
{
    if(sem->getBoughtCoins()>=200&& sem->getBigBonusReceived()){
        sem->setBigBonusReceived(true);
        Shop::GetInstanse()->setCountCoints(Shop::GetInstanse()->getCountCoints()+40);
    }
}
