#ifndef STATES_H
#define STATES_H
class AudioPlayer;

class States
{
protected:
    AudioPlayer* context;
public:
    States();
    States(AudioPlayer*);
    void setContext(AudioPlayer *value);
    virtual void clickLock()=0;
    virtual void clickPlay()=0;
    void clickNext();
    void clickPrevious();
};


class ReadyState:public States{
public:
    ReadyState(AudioPlayer* _context):States(_context){}
    ReadyState(){}
    void clickLock() override;
    void clickPlay() override;
};

class PlayingState: public States{
public:
    PlayingState(AudioPlayer* _context):States(_context){}
    PlayingState(){}
    void clickLock() override;
    void clickPlay() override;
};

#endif // STATES_H
