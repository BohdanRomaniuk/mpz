#ifndef COMAND_H
#define COMAND_H
#include"server.h"

class Comand
{
public:
    Comand();
    virtual bool Execute()=0;
};

class RegisterComand:public Comand{

  QString UserName;
  QString Pass;
  Server *receiver;
  public:
  RegisterComand(QString, QString, Server*);
  bool Execute() override;
};

class AuthorizationComand:public Comand{

  QString UserName;
  QString Pass;
  Server *receiver;
public:
  AuthorizationComand(){}
  AuthorizationComand(QString, QString, Server*);
  bool Execute() override;
};
#endif // COMAND_H
