#ifndef AUDIOPLAYER_H
#define AUDIOPLAYER_H
#include"QMediaPlayer"
#include"states.h"
#include"QMediaPlaylist"
class AudioPlayer
{
    States* state;
    //volume playlist curSong
    QMediaPlaylist *playlist;
    QMediaPlayer *player;
    int Volume=50;
    int CurrSong=1;
public:
    AudioPlayer();
    AudioPlayer(States*);
    States* ChangeState(States*);

    void clickLock();
    void clickNext();
    void clickPlay();
    void clickPrevious();

    void startPlayback();
    void pausePlayback();
    void nextSong();
    void previousSong();
};


#endif // AUDIOPLAYER_H
