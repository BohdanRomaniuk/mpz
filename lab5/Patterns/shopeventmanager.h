#ifndef SHOPEVENTMANAGER_H
#define SHOPEVENTMANAGER_H

#include"observer.h"
#include"QVector"

class ShopEventManager
{
    int BoughtCoins=0;
    bool SmallBonusReceived=false;
    bool BigBonusReceived=false;
    QVector<BonusListener*> Observers;
public:
    ShopEventManager();
    void AttachObserver(BonusListener*);
    void DettachObserver(BonusListener*);
    void Notify();
    int getBoughtCoins() const;
    void setBoughtCoins(int value);
    bool getSmallBonusReceived() const;
    void setSmallBonusReceived(bool value);
    bool getBigBonusReceived() const;
    void setBigBonusReceived(bool value);
};

#endif // SHOPEVENTMANAGER_H
