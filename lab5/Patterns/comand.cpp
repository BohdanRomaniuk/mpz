#include "comand.h"

Comand::Comand()
{

}

RegisterComand::RegisterComand(QString username,QString pass, Server * _receiver){
    UserName=username;
    Pass=pass;
    receiver=_receiver;
}

bool RegisterComand::Execute()
{
    return receiver->AddUser(UserName,Pass);
}

AuthorizationComand::AuthorizationComand(QString username,QString pass, Server *_receiver)
{
    UserName=username;
    Pass=pass;
    receiver=_receiver;
}

bool AuthorizationComand::Execute()
{
   return receiver->FindUser(UserName,Pass);
}
