#include "shop.h"
Shop* Shop::Instanse=nullptr;
QMutex* Shop::lock=new QMutex;
void Shop::setCountCoints(int value)
{
    CountCoints = value;
}

int Shop::getCountCoints() const
{
    return CountCoints;
}

ShopEventManager *Shop::getShopEventManager() const
{
    return shopEventManager;
}

Shop::Shop()
{
    shopEventManager=new  ShopEventManager;
}
Shop* Shop::GetInstanse(){
if (Instanse == nullptr)
   {
    lock->lock();
    if(Instanse==nullptr)
        Instanse=new Shop;
    lock->unlock();
}
 return Instanse;
}

void Shop::AddCoins(int coins)
{
    CountCoints+=coins;
    shopEventManager->setBoughtCoins(shopEventManager->getBoughtCoins()+coins);
    shopEventManager->Notify();
}

void Shop::UpdataCoinsLabel(QLabel * label)
{
    label->setText(QString::number(CountCoints));
}

Memento *Shop::Save(QString File)
{
    return new Memento(File);
}

void Shop::Load(Memento *memento)
{
    memento->Load();
}






