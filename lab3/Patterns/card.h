#ifndef CARD_H
#define CARD_H

#include"flyweight.h"
#include"timer.h"


static int TimeBonusEzLevel =50000;
static int TimeBonusHLevel =10000;

static int TimeBadEzLevel =-30000;
static int TimeBadHLevel =-40000;

class Engine;

class Card{
protected:
QPixmap *CardPicture;
public:
    virtual Card* FlipCard(Engine*)=0;
    virtual Card* Clone()=0;
    virtual Card* ShowPicture(QLabel *);
};
class StandartCard :public Card
{
public:

};

class BonusCard:public Card
{
    virtual void GetBonus(Engine*)=0;
};

class ExtraTimeBonusEz :public BonusCard{
public:

    ExtraTimeBonusEz(){ CardPicture=Flyweight::GetInstanse()->GetBonusCardPicture();}
     void GetBonus(Engine *Eng) override;
     Card* FlipCard(Engine *eng) override {GetBonus(eng);return this;}
     Card* Clone() override {return new ExtraTimeBonusEz();}

};
class ExtraTimeBonusH :public BonusCard{
public:
    ExtraTimeBonusH(){ CardPicture=Flyweight::GetInstanse()->GetBonusCardPicture();}
     void GetBonus(Engine *Eng);
     Card* FlipCard(Engine *Eng){GetBonus(Eng);return this;}
     Card* Clone(){return new ExtraTimeBonusH();}

};
/*class DelCardBonusEz :public BonusCard{

};
class DelCardBonusH :public BonusCard{

};*/


class BadCard:public Card
{
  virtual void GetBadCard(Engine*)=0;
};

class ReduseExtraTimeEz :public BadCard
{
public:
    ReduseExtraTimeEz(){ CardPicture=Flyweight::GetInstanse()->GetBadCardPicture();}
    void GetBadCard(Engine *Eng);
    Card* FlipCard(Engine *Eng){GetBadCard(Eng);return this;}
    Card* Clone(){return new ReduseExtraTimeEz();}

};


class ReduseExtraTimeH :public BadCard
{
public:
    ReduseExtraTimeH(){ CardPicture=Flyweight::GetInstanse()->GetBadCardPicture();}
    void GetBadCard(Engine *Eng);
    Card* FlipCard(Engine *Eng){GetBadCard(Eng);return this;}
    Card* Clone(){return new ReduseExtraTimeH();}

};


class CardDecorator :public Card{
protected:  Card *card;
public:
     CardDecorator(Card* card){this->card=card;}
     Card* FlipCard(Engine* Eng){ card->FlipCard(Eng);return this;}
     Card* Clone(){ card->Clone();return this;}
     Card* ShowPicture(QLabel *label){card->ShowPicture(label);return this;}
     void SetTime(int);
     void BackTime(int);
};

class X4 :public CardDecorator{
public:
    X4(Card *card): CardDecorator(card){}
    Card* FlipCard(Engine* Eng){
         SetTime(4);
         card->FlipCard(Eng);
         BackTime(4);
         return this;
    }
};
class X2 :public CardDecorator{
    X2(Card *card): CardDecorator(card){}
public:
    Card* FlipCard(Engine* Eng){
         SetTime(2);
         card->FlipCard(Eng);
         BackTime(2);
         return this;
    }
};
#endif // CARD_H
