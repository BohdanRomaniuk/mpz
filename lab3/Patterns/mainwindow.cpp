#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSignalMapper>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QSignalMapper* signalMapper = new QSignalMapper (this) ;
    facade=new ShopControllFacade;

    connect (ui->Buy20, SIGNAL(clicked()), signalMapper, SLOT(map())) ;
    connect (ui->Buy30, SIGNAL(clicked()), signalMapper, SLOT(map())) ;
    connect (ui->Buy40, SIGNAL(clicked()), signalMapper, SLOT(map())) ;

    signalMapper -> setMapping (ui->Buy20, 20) ;
    signalMapper -> setMapping (ui->Buy30, 30) ;
    signalMapper -> setMapping (ui->Buy40, 40) ;

    connect(ui->EazyLevel,SIGNAL(clicked()),this,SLOT(CreateEazyLevel()));
    connect(ui->HardLevel,SIGNAL(clicked()),this,SLOT(CreateHardLevel()));

    connect(signalMapper,SIGNAL(mapped(int)),this,SLOT(on_BuyButton_click(int)));


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::CreateEazyLevel()
{
    ui->stackedWidget->setCurrentIndex(ui->stackedWidget->currentIndex()-1);
    LevelsFactory* Factory=new EazyLevelFactory;
    engine=Engine::GetInstanse();
    engine->Start(Factory);
    engine->GenerareList(ui->listWidget);
    engine->setTimeLabel(ui->Time);
    engine->StartTimer();
}

void MainWindow::on_ExitButton_clicked()
{
    exit(0);
}

void MainWindow::on_StartGameButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(ui->stackedWidget->currentIndex()-1);

}

void MainWindow::CreateHardLevel()
{
    ui->stackedWidget->setCurrentIndex(ui->stackedWidget->currentIndex()-1);
    LevelsFactory* Factory=new HardLevelFactory;
    engine=Engine::GetInstanse();
    engine->Start(Factory);
    engine->GenerareList(ui->listWidget);
    engine->setTimeLabel(ui->Time);
    engine->StartTimer();
}

void MainWindow::on_ChoseButton_clicked()
{
    delete  engine->GetCard(ui->listWidget->currentRow())->FlipCard(engine)->ShowPicture(ui->LastCardPicture);
    engine->DeleteCard(ui->listWidget->currentRow());
    ui->listWidget->takeItem(ui->listWidget->currentRow());

}

void MainWindow::on_BuyButton_click(int coins)
{
  facade->BuyCoins(coins,ui->CoinsCount);
}

void MainWindow::on_OpenShopButton_clicked()
{
     ui->stackedWidget->setCurrentIndex(ui->stackedWidget->currentIndex()-3);
}

void MainWindow::on_pushButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(ui->stackedWidget->currentIndex()+3);
}
