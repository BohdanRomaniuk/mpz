#ifndef TABLE_H
#define TABLE_H
#include<QString>
#include <QMap>
using namespace std;
class Table
{
    static QMap<QString,int> VarTable;
public:
    Table();
    static bool isExists(QString key);
    static int GetElemet(QString key);
    static void SetElement(QString key,int val);

};

#endif // TABLE_H
