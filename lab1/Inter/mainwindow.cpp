#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QStringList"
#include "Lexer.h"
#include "parser.h"
#include"optimizer.h"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    pL=new Lexer;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_RunButton_clicked()
{
  try {
    ui->DebugEdit->setText("");
    ui->treeWidget->clear();

    pL->SetText(ui->Code->toPlainText());
    pL->StartLA(ui->DebugEdit);

    pParser=new Parser(pL->GetTokens());
    Statment *AllStatments=pParser->StartParsing();

    pOptimizer=new Optimizer;
    Statment *OptProg=pOptimizer->Optimize(AllStatments);

    OptProg->Execute();
    OptProg->PaintTree(ui->treeWidget);
  }
   catch (std::runtime_error er) {
      QMessageBox::critical(this,"",er.what());
  }
}
