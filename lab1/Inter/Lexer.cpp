#include "Lexer.h"
#include <cctype>
#include <QMessageBox>
#include <exception>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "mainwindow.h"
void Lexer::SkipSpace(){
    while(Str[Cpos].isSpace()||Str[Cpos]=='\n')
          NextPos();
}

QString Lexer::Number(){
    QString Digit;
    while(Str[Cpos].isDigit()){
        Digit.append(Str[Cpos]);
        NextPos();
    }
    IsCorrectWrd();
    return Digit;
}

QString Lexer::Word(){
    QString Wrd;
    while(Str[Cpos].isLetterOrNumber()){
     Wrd.append(Str[Cpos]);
     NextPos();
    }
    return Wrd;
}

void Lexer::NextPos(){
    if(Cpos<Str.length())
        Cpos++;
}

QString Lexer::String(){
    QString string;
    NextPos();
    while(Str[Cpos]!="\""){
        string.append(Str[Cpos]);
        NextPos();
        if(Cpos==Str.size()){
            throw  std::runtime_error("missed \"");
        }
    }
    NextPos();
    return string;
}

void Lexer::IsCorrectWrd(){
    if(Str[Cpos].isLetter())
        throw std::runtime_error("Uncorrect ID");
}

void Lexer::StartLA(QTextEdit *ResultWindow){
    Cpos=0;
    Tokens.clear();
    while(Cpos<Str.length()){
        if(Str[Cpos].isDigit()){
           QString Digit=Number();
           Tokens.push_back(Token(Token::Type::number,Digit));
         }
        else if(Str[Cpos].isLetterOrNumber()){
          QString Wrd=Word();
          if(KeyWords.contains(Wrd)){
              if(Wrd=="if")
                  Tokens.push_back(Token(Token::Type::kwIf,Wrd));
              else if (Wrd=="while") {
                  Tokens.push_back(Token(Token::Type::kwWhile,Wrd));
              }
              else if (Wrd=="click") {
                  Tokens.push_back(Token(Token::Type::kwClick,Wrd));
              }
              else if (Wrd=="drag") {
                  Tokens.push_back(Token(Token::Type::kwDrag,Wrd));
              }
              else if (Wrd=="OpenBatFile") {
                  Tokens.push_back(Token(Token::Type::kwOpenBatFile,Wrd));
              }
              else if (Wrd=="PressButton") {
                  Tokens.push_back(Token(Token::Type::kwPressButton,Wrd));
              }
              else if (Wrd=="NewThread") {
                  Tokens.push_back(Token(Token::Type::kwNewThread,Wrd));
              }
          }
          else {
           Tokens.push_back(Token(Token::Type::var,Wrd));
           }
        }
        else if(Str[Cpos].isSpace()){
            SkipSpace();
        }
        else if(Str.at(Cpos)=="\""){
            QString string=String();
            Tokens.push_back(Token(Token::Type::string,string));
        }
        else if(Separators.contains(Str.at(Cpos))){
            if(Str.at(Cpos)==";"){
                    Tokens.push_back(Token(Token::Type::sepSC,Str.at(Cpos)));
                    NextPos();
            }
            else if (Str.at(Cpos)==":") {
                Tokens.push_back(Token(Token::Type::sepDP,Str.at(Cpos)));
                NextPos();
            }
            else if (Str.at(Cpos)==",") {
                Tokens.push_back(Token(Token::Type::sepComa,Str.at(Cpos)));
                NextPos();
            }
                }
        else if(Operators.contains(Str.at(Cpos))){
            if(Str.at(Cpos)=="+"){
                    Tokens.push_back(Token(Token::Type::opPlus,Str.at(Cpos)));
                    NextPos();
            }
            else if (Str.at(Cpos)=="-") {
                Tokens.push_back(Token(Token::Type::opMinus,Str.at(Cpos)));
                NextPos();
            }
            else if (Str.at(Cpos)=="=") {
                Tokens.push_back(Token(Token::Type::opEQ,Str.at(Cpos)));
                NextPos();
            }
            else if (Str.at(Cpos)==">") {
                Tokens.push_back(Token(Token::Type::opGT,Str.at(Cpos)));
                NextPos();
            }
            else if (Str.at(Cpos)=="<") {
                Tokens.push_back(Token(Token::Type::opLT,Str.at(Cpos)));
                NextPos();
            }

                }
        else {
            throw std::runtime_error("Undefined symbol");
        }

   }
    PrintTokens(ResultWindow);

    if(Cpos>Str.length())
        Tokens.push_back(Token(Token::Type::eof,"eof"));
}
void Lexer::PrintTokens(QTextEdit *DebugWindow){
    QString TokenName;
    for (int i=0;i<Tokens.size();++i) {
        switch(Tokens[i].ID) {
              case Token::Type::sepDP: TokenName="SepDP"; break;
              case Token::Type::sepSC: TokenName="SepSC"; break;
              case Token::Type::sepComa: TokenName="SepComa"; break;
              case Token::Type::var: TokenName="Var";break;
              case Token::Type::number:TokenName="Number";break;
              case Token::Type::kwIf:TokenName="kwIf";break;
              case Token::Type::kwWhile:TokenName="kwWhile";break;
              case Token::Type::kwClick:TokenName="kwClick";break;
              case Token::Type::kwOpenBatFile:TokenName="kwOpenBat";break;
              case Token::Type::kwDrag:TokenName="kwDrag";break;
              case Token::Type::kwPressButton:TokenName="kwPressButton";break;
              case Token::Type::kwNewThread:TokenName="kwNewThread";break;
              case Token::Type::opPlus:TokenName="opPlus";break;
              case Token::Type::opMinus:TokenName="opMinus";break;
              case Token::Type::opEQ:TokenName="opEQ";break;
              case Token::Type::opGT:TokenName="opGT";break;
              case Token::Type::opLT:TokenName="opLT";break;
              case Token::Type::string:TokenName="String";break;
           }
        DebugWindow->textCursor().insertText("<"+TokenName+"|"+Tokens[i].Value+"> ");
    }
}
