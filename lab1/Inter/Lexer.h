#ifndef INTER_H
#define INTER_H
#include"string"
#include"QString"
#include"QVector"
#include"QTextEdit"

class Token{
  public:
    QString Value;

    enum Type{
      sepComa,sepDP,sepSC,kwClick,kwDrag,kwOpenBatFile,kwWhile,kwPressButton,kwNewThread,kwIf,number,string,var,opPlus,opMinus,opEQ,opLT,opGT,eof
    } ID;

    Token(Token::Type ttype,QString token){
      ID=ttype;
      Value=token;
    }
};

class Lexer{
    unsigned Cpos=0;
    QString Str;
    QVector<QString> KeyWords={"click","drag","OpenBatFile","PressButton","NewThread","while","if"};
    QVector<QString> Separators={":",";",","};
    QVector<QString> Operators={"+","-","=","<",">"};
    QVector<Token> Tokens;
    QString Number();
    void SkipSpace();
    QString Word();
    QString String();
    void NextPos();
    void IsCorrectWrd();
  public:
   void SetText(QString Text){Str=Text;}
   void StartLA(QTextEdit *);
   QVector<Token> GetTokens(){return Tokens;}
   void PrintTokens(QTextEdit *);
}static *pL;


#endif // INTER_H
