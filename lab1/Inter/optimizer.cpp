#include "optimizer.h"
#include "parser.h"
Optimizer::Optimizer()
{

}
Statment* Optimizer::Optimize(Statment* st){
  return st->CallOptimizer(this);
}

Statment* Optimizer::ChangeSt(WhileStatment * st){
    if(dynamic_cast<NumbExp*>(st->GetExpr())){
        if(st->GetExpr()->eval()->retNumb())
            throw std::runtime_error("");
        else {
            return nullptr;
        }
    }
    return st;
}

Statment* Optimizer::ChangeSt(ifStatment * st){
    if(dynamic_cast<NumbExp*>(st->GetExpr())){
        if(st->GetExpr()->eval()->retNumb())
            return st->GetStm();
        else {
            return nullptr;
        }
    }
    return st;
}

Statment* Optimizer::ChangeSt(AllStatment * st){
    AllStatment *OptProg=new AllStatment;
    Statment *tmp;
    for (int i=0;i<st->GetSize();++i) {
        tmp=st->GetAllStm(i)->CallOptimizer(this);
        if(tmp!=nullptr)
            OptProg->Add(tmp);
    }
    return OptProg;
}
