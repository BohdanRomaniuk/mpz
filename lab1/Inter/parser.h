#ifndef PARSER_H
#define PARSER_H
#include <Lexer.h>
#include<table.h>
#include<QTreeWidget>
#include<QTreeWidgetItem>
#include"windows.h"
#include<cstdlib>
#include<QDebug>
#include<QThread>
#include<QMessageBox>
#include<QMutex>
#include<QProcess>
#include<optimizer.h>
static QMutex Mutex;

class TypeofVar{
public:
    virtual int retNumb()=0;
    virtual QString retStr()=0;

};

class Number:public TypeofVar{
    int val;
public:
    Number(int numb){this->val=numb;}
    int retNumb(){return val;}
    QString retStr(){return QString::number(val);}
};

class String:public TypeofVar{
  QString val;
public:
  String(QString str){this->val=str;}
  int retNumb(){return val.toInt();}
  QString retStr(){return val;}
};

class Expression{
public:
    virtual TypeofVar* eval()=0;
    virtual void PaintTree(QTreeWidget *,QTreeWidgetItem* it=nullptr)=0;
};

class NumbExp:public Expression{
    int val;
public:
    NumbExp(int Numb){this->val=Numb;}
    TypeofVar* eval(){return new Number(val);}
    void PaintTree(QTreeWidget *Tree,QTreeWidgetItem *parent){
        QTreeWidgetItem* item = new QTreeWidgetItem;
        item->setText(0,QString::number(val));
        if(parent != nullptr){
            parent->addChild(item);
        }
        Tree->addTopLevelItem(item);
    }
};

class StrExp:public Expression{
    QString val;
public:
    StrExp(QString str){this->val=str;}
    TypeofVar* eval(){return new String(val);}
    void PaintTree(QTreeWidget *Tree,QTreeWidgetItem *parent){
        QTreeWidgetItem* item = new QTreeWidgetItem;
        item->setText(0,val);
        if(parent != nullptr){
            parent->addChild(item);
        }
        Tree->addTopLevelItem(item);
    }
};

class BinExp:public Expression{
    Expression *LeftExp,*RightExp;
    char Operation;
public:
    BinExp(Expression *l,char op,Expression *r){
        this->LeftExp=l;
        this->RightExp=r;
        this->Operation=op;
    }

    TypeofVar* eval(){
        switch (Operation) {
        case '+': return new Number(LeftExp->eval()->retNumb()+RightExp->eval()->retNumb());
        case '-': return new Number(LeftExp->eval()->retNumb()-RightExp->eval()->retNumb());

        }
    }

    void PaintTree(QTreeWidget *Tree,QTreeWidgetItem *parent){
        QTreeWidgetItem* item = new QTreeWidgetItem;
        item->setText(0,QString(QChar::fromLatin1(Operation)));
        if(parent != nullptr){
            parent->addChild(item);

        }
        else {
            Tree->addTopLevelItem(item);
        }
        LeftExp->PaintTree(Tree,item);
        RightExp->PaintTree(Tree,item);

    }
};



class Statment{
public:
    virtual void Execute()=0;
    virtual Statment* CallOptimizer(Optimizer *)=0;
    virtual Expression* GetExpr()=0;
    virtual Statment* GetStm()=0;
    virtual void PaintTree(QTreeWidget *Tree,QTreeWidgetItem *item=nullptr)=0;
};

class AssignStatment:public Statment{
    QString Variable;
    Expression *ex;
public:
    AssignStatment(QString var,Expression *ex){this->Variable=var;this->ex=ex;}
    void Execute(){
        int Expres=ex->eval()->retNumb();
        Table::SetElement(Variable,Expres);
    }

    Statment* CallOptimizer(Optimizer *op){
        return this;
    }

    Expression* GetExpr(){return ex;}
    Statment* GetStm(){}

    void PaintTree(QTreeWidget *Tree,QTreeWidgetItem *parent){
        QTreeWidgetItem* item = new QTreeWidgetItem;
        item->setText(0,"=");
        if(parent != nullptr){
            parent->addChild(item);
        }
        else {
             Tree->addTopLevelItem(item);
        }
        QTreeWidgetItem* variable = new QTreeWidgetItem;
        variable->setText(0,Variable);
        item->addChild(variable);
        ex->PaintTree(Tree,item);
    }

};

class ifExpression:public Expression{
    Expression *LeftExp,*RightExp;
    char Operation;
public:
    ifExpression(Expression *l,char op,Expression *r){
        this->LeftExp=l;
        this->RightExp=r;
        this->Operation=op;
    }
    TypeofVar* eval();

    void PaintTree(QTreeWidget *Tree,QTreeWidgetItem *parent){
        QTreeWidgetItem* item = new QTreeWidgetItem;
        item->setText(0,QString(QChar::fromLatin1(Operation)));
        if(parent != nullptr){
            parent->addChild(item);
        }
        else{ Tree->addTopLevelItem(item);}
        LeftExp->PaintTree(Tree,item);
        RightExp->PaintTree(Tree,item);
    }
};

class ifStatment:public Statment{
    Expression *ex;
    Statment *ifstatment;
public:
    ifStatment(Expression *ex, Statment *ifstatment){
        this->ex=ex;
        this->ifstatment=ifstatment;
    }

    void Execute();

    Statment* CallOptimizer(Optimizer *op){
        return  op->ChangeSt(this);
    }

    Expression* GetExpr(){return ex;}
    Statment* GetStm(){return ifstatment;}

    void PaintTree(QTreeWidget *Tree,QTreeWidgetItem *parent){
        QTreeWidgetItem* item = new QTreeWidgetItem;
        item->setText(0,"if");

        if(parent != nullptr){
            parent->addChild(item);
        }
        else{Tree->addTopLevelItem(item);}
        ex->PaintTree(Tree,item);
        ifstatment->PaintTree(Tree,item);
    }
};

class WhileStatment:public Statment{
    Expression *cond;
    Statment *statment;
public:
    WhileStatment( Expression *cond,Statment *statment){this->cond=cond;this->statment=statment;}
    void Execute(){
        while (cond->eval()->retNumb()!=0) {
            statment->Execute();
        }
    }

    Statment* CallOptimizer(Optimizer *op){
        return  op->ChangeSt(this);
    }
    Expression* GetExpr(){return cond;}
    Statment* GetStm(){return statment;}

    void PaintTree(QTreeWidget *Tree,QTreeWidgetItem *parent){
        QTreeWidgetItem* item = new QTreeWidgetItem;
        item->setText(0,"while");
        if(parent != nullptr){
            parent->addChild(item);
        }
        else{Tree->addTopLevelItem(item);}
        cond->PaintTree(Tree,item);
        statment->PaintTree(Tree,item);
    }
};
class VarExpression :public Expression{
    QString var;
public:
    VarExpression(QString var){this->var=var;}

    TypeofVar *eval(){
        if(Table::isExists(var))//check this
            return new Number(Table::GetElemet(var));
        else {
            throw std::runtime_error("Undefined var");
        }
    }

    void PaintTree(QTreeWidget *Tree,QTreeWidgetItem *parent){
        QTreeWidgetItem* item = new QTreeWidgetItem;
        item->setText(0,var);
        if(parent != nullptr){
            parent->addChild(item);
        }
        else
        Tree->addTopLevelItem(item);
        QTreeWidgetItem* numb = new QTreeWidgetItem;
        numb->setText(0,QString::number(Table::GetElemet(var)));
        item->addChild(numb);
    }
};


class Function{
public:
    virtual TypeofVar* execute(QVector<TypeofVar*>)=0;
    virtual bool isCorrectCountArg(int)=0;
};

class FuncTable
{
public:
    static QMap<QString,Function*>funcTable;
    static bool isExists(QString key){
      return FuncTable::funcTable.contains(key);
   }

    static Function* GetElemet(QString key){
       if(isExists(key))
          return FuncTable::funcTable[key];
       else {
          runtime_error("Undefined function");
       }

   }

    static void SetElement(QString key,Function* func){
      FuncTable::funcTable.insert(key,func);
   }
};
class Click:public Function{
public:
    bool isCorrectCountArg(int ArgSize) override{
        if(ArgSize!=2)
            throw  runtime_error("Function Click use 2 arguments");
        else return 1;
    }

    TypeofVar* execute(QVector<TypeofVar*> arg){
            isCorrectCountArg(arg.size());
            Mutex.lock();
            Sleep(1000);
            SetCursorPos(arg[0]->retNumb(),arg[1]->retNumb());
            Sleep(1000);
            mouse_event(MOUSEEVENTF_LEFTDOWN,0,0,0,0);
            mouse_event(MOUSEEVENTF_LEFTUP,0,0,0,0);
            qDebug()<<"Clicked";
            Mutex.unlock();
     }
};

class Drag:public Function{
public:
    bool isCorrectCountArg(int ArgSize) override{
        if(ArgSize!=4)
            throw  runtime_error("Function Drug use 4 arguments");
        else return 1;
    }

    TypeofVar* execute(QVector<TypeofVar*> arg){
            isCorrectCountArg(arg.size());
            Mutex.lock();
            Sleep(1000);
            SetCursorPos(arg[0]->retNumb(),arg[1]->retNumb());
            mouse_event(MOUSEEVENTF_LEFTDOWN,0,0,0,0);
            Sleep(100);
            SetCursorPos(arg[2]->retNumb(),arg[3]->retNumb());
            Sleep(2000);
            mouse_event(MOUSEEVENTF_LEFTUP,0,0,0,0);
            qDebug()<<arg.size();
            Mutex.unlock();
    }
};
class OpenBatFile:public Function{
public:
    bool isCorrectCountArg(int ArgSize) override{
        if(ArgSize!=1)
            throw   runtime_error("Function OpenBatFile use 1 argument");
        else return 1;
    }

    TypeofVar* execute(QVector<TypeofVar*> arg){       
        system(arg[0]->retStr().toStdString().c_str());
    }
};
class PressButton:public Function{
public:
    bool isCorrectCountArg(int ArgSize) override{
        if(ArgSize!=1)
            throw  runtime_error("Function PressButton use 1 argument");
        else return 1;
    }

    TypeofVar* execute(QVector<TypeofVar*> arg){
           keybd_event(arg[0]->retNumb(),0x45,KEYEVENTF_EXTENDEDKEY,0);
           Sleep(100);
           keybd_event(arg[0]->retNumb(),0x45,KEYEVENTF_KEYUP,0);
    }

};
class Thread:public QThread {
    QVector<TypeofVar*> arg;
public:
    Thread(QVector<TypeofVar*> arg){
        this->arg=arg;
    }

    void isCorrectArg(){
         FuncTable::GetElemet(arg[0]->retStr())->isCorrectCountArg(arg.size()-1);
    }

protected:
    void run() override{
            QString FuncName=arg[0]->retStr();
            arg.pop_front();
            FuncTable::GetElemet(FuncName)->execute(arg);
    }
};
class NewThread:public Function{
public:
    bool  isCorrectCountArg(int arg){
        if(arg>5||arg==0) throw runtime_error("Too mach arguments for New Thread");
         else return 1;
      }

    TypeofVar* execute(QVector<TypeofVar*> arg){
            isCorrectCountArg(arg.size());
            Thread *NewTh=new Thread(arg);
            NewTh->isCorrectArg();
            NewTh->start();
    }
};


class FunctionExpression:public Expression{
    QString FunctionName;
    QVector<Expression*> Arguments;
public:
    FunctionExpression(QString name){this->FunctionName=name;}
    FunctionExpression(QString name, QVector<Expression*> arg){
        this->FunctionName=name;
        this->Arguments=arg;
    }

    TypeofVar* eval(){
        QVector<TypeofVar*>arg;
        for (int i=0;i<Arguments.size();++i) {
            arg.push_back(Arguments[i]->eval());
        }       
        return FuncTable::GetElemet(FunctionName)->execute(arg);
    }

    QString GetFuncName(){return FunctionName;}

    void PaintTree(QTreeWidget *Tree,QTreeWidgetItem *parent){
        for (int i=0;i<Arguments.size();++i) {
            QTreeWidgetItem *item=new QTreeWidgetItem;
            item->setText(0, Arguments[i]->eval()->retStr());
            if(parent!=nullptr){
                parent->addChild(item);
            }
            else {
                Tree->addTopLevelItem(item);
            }
        }
    }
    void AddNewArg(Expression* NewArg){Arguments.push_back(NewArg);}
};

class FunctionStatment:public Statment{
    FunctionExpression *expr;
public:

    FunctionStatment(FunctionExpression* exp){this->expr=exp;}
    void Execute(){
        expr->eval();
    }

    Statment* CallOptimizer(Optimizer *op){
        return  this;
    }

     Expression* GetExpr(){return expr;}
     Statment* GetStm(){}

     void PaintTree(QTreeWidget *Tree,QTreeWidgetItem *parent)
     {
      QTreeWidgetItem *item=new QTreeWidgetItem;
      item->setText(0,"Function"+expr->GetFuncName());
      if(parent!=nullptr){
          parent->addChild(item);
      }
      else {
          Tree->addTopLevelItem(item);
      }
      expr->PaintTree(Tree,item);
     }
};

class AllStatment :public Statment{
    QVector<Statment*> allstatment;
public:

    void Add(Statment * newStetment){
        allstatment.push_back(newStetment);
    }

    void Execute(){
        for (int i=0;i<allstatment.size();++i) {
            allstatment[i]->Execute();
        }
    }

    Statment* CallOptimizer(Optimizer *op){
        return  op->ChangeSt(this);
    }

    Expression* GetExpr(){}
    Statment* GetStm(){}
    Statment* GetAllStm(int i){return allstatment[i];}
    int GetSize(){return allstatment.size();}

    void PaintTree(QTreeWidget *Tree,QTreeWidgetItem *parent){
        if(parent != nullptr){

                for (int i=0;i<allstatment.size();++i) {
                     allstatment[i]->PaintTree(Tree,parent);
                }
        }
        else{
            for (int i=0;i<allstatment.size();++i) {
                 allstatment[i]->PaintTree(Tree);
            }
        }
    }
};

class Parser
{   int Cpos=0;
    QVector<Token> Tokens;
public:

    Parser(QVector<Token> Tokens){
        this->Tokens=Tokens;
        FuncTable::SetElement("click",new Click);
        FuncTable::SetElement("drag",new Drag);
        FuncTable::SetElement("OpenBatFile",new OpenBatFile);
        FuncTable::SetElement("PressButton",new PressButton);
        FuncTable::SetElement("NewThread",new NewThread);
    }

    Token GetToken(int pos);
    bool Match(Token::Type type);
    Statment *statment();
    Statment* Ifst();
    Statment* WhileSt();
    Statment* Assignstatment();
    Expression *expression(){return conditional();}
    Expression *conditional();
    Expression *Prim();
    Expression *PlusMinus();
    Statment *StartParsing();
    Statment* Func(QString FuncName);
    Token MatchAndGet(Token::Type Type);

}static *pParser;
#endif // PARSER_H
