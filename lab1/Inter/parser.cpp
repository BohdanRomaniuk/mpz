#include "parser.h"

TypeofVar* ifExpression::eval(){
        switch (Operation) {
           case '>': return new Number(LeftExp->eval()->retNumb()>RightExp->eval()->retNumb());
           case '<': return new Number(LeftExp->eval()->retNumb()<RightExp->eval()->retNumb());
         }
}

void ifStatment::Execute(){
    int result=ex->eval()->retNumb();
    if(result)
        ifstatment->Execute();
}

Token Parser::GetToken(int pos){
    int position=Cpos+pos;
    if(position<Tokens.size())
        return Tokens[position];
    else {
        return Token(Token::Type::eof,"EOF");
    }
}

bool Parser::Match(Token::Type type){
    if(type==GetToken(0).ID){
        Cpos++;
        return true;
    }
    else{
        return false;
    }
}

Expression* Parser::Prim(){
    Token Cur=GetToken(0);
    if(Match(Token::Type::number)){
        return new NumbExp(Cur.Value.toInt());
    }
    else if (Match(Token::Type::string)) {
        return new StrExp(Cur.Value);
    }
    else if (Match(Token::Type::var)) {
        return new VarExpression(Cur.Value);
    }
    else {
        throw std::runtime_error("Undefined var");
    }
}

Expression* Parser::PlusMinus(){
    Expression* res = Prim();
    while(true){
        if(Match(Token::Type::opPlus)){
            res = new BinExp(res,'+', Prim());
            continue;
        }
        else if(Match(Token::Type::opMinus)){
            res = new BinExp(res,'-', Prim());
            continue;
        }
        break;
    }
    return res;
}

Statment* Parser::Func(QString name){
    FunctionExpression* exp=new FunctionExpression(name);
    Match(Token::Type::sepDP);
    while (!Match(Token::Type::sepSC)) {
        exp->AddNewArg(expression());
        Match(Token::Type::sepComa);
    }
    return new FunctionStatment(exp);
}

Statment *Parser::statment(){

    if(Match(Token::Type::kwIf)){
       if(Match(Token::Type::sepDP))
        return Ifst();
    }
    if(Match(Token::Type::kwWhile)){
            Match(Token::Type::sepDP);
            return WhileSt();
        }

     if(GetToken(0).ID==Token::Type::kwClick){
         return  Func(MatchAndGet(Token::Type::kwClick).Value);
     }
     if(GetToken(0).ID==Token::Type::kwDrag){
        return Func(MatchAndGet(Token::Type::kwDrag).Value);
     }
     if(GetToken(0).ID==Token::Type::kwOpenBatFile){
        return Func(MatchAndGet(Token::Type::kwOpenBatFile).Value);
     }
     if(GetToken(0).ID==Token::Type::kwPressButton){
        return Func(MatchAndGet(Token::Type::kwPressButton).Value);
     }
     if(GetToken(0).ID==Token::Type::kwNewThread){
        return Func(MatchAndGet(Token::Type::kwNewThread).Value);
     }
    return Assignstatment();

}

Statment *Parser::Assignstatment(){
    if(GetToken(0).ID == Token::Type::var && GetToken(1).ID==Token::Type::opEQ){
        QString var = GetToken(0).Value;
        if(Match(Token::Type::var)){
            if(Match(Token::Type::opEQ)){
                Statment* statment = new AssignStatment(var,expression());
                return statment;
            }
        }
    }
    \
    throw std::runtime_error("Error building");
}

Token Parser::MatchAndGet(Token::Type Type)
{
    Token cur = GetToken(0);
    if(cur.ID == Type){
        Cpos++;
        return cur;
    }

}

Expression* Parser::conditional(){
    Expression* result = PlusMinus();
    while(true){
        if(Match(Token::Type::opGT)){
            return new ifExpression(result,'>',PlusMinus());
        }
        else if(Match(Token::Type::opLT)){
            return new ifExpression(result,'<',PlusMinus());
        }
        break;
    }
    return result;
}

Statment *Parser::Ifst(){
        Expression* condition = expression();
        Statment* ifSt=statment();
        return new ifStatment(condition,ifSt);
}

Statment *Parser::WhileSt(){
    Expression* condition = expression();
    Statment* St=statment();
    return new WhileStatment(condition,St);
}

Statment* Parser::StartParsing(){
    AllStatment *allStatments=new AllStatment;
    while (!Match(Token::Type::eof)) {
        allStatments->Add(statment());       
        Match(Token::Type::sepSC);
    }
    return allStatments;
}

QMap<QString,Function*>FuncTable::funcTable;
