#ifndef OPTIMIZER_H
#define OPTIMIZER_H

class Statment;
class AllStatment;
class ifStatment;
class WhileStatment;
class NumbExp;

class Optimizer
{
public:
    Optimizer();
    Statment* Optimize(Statment *);
    Statment* ChangeSt(WhileStatment *);
    Statment* ChangeSt(ifStatment *);
    Statment* ChangeSt(AllStatment *);

}static *pOptimizer;

#endif // OPTIMIZER_H
