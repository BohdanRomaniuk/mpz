#ifndef LEVELS_H
#define LEVELS_H
#include"card.h"
#include <QObject>

class LevelsFactory
{
public:

 virtual  BonusCard* GetBonusTimeCard()=0;
 virtual  BadCard* GetBadTimeCard()=0;
 virtual  Timer* GetTimer()=0;

};

class EazyLevelFactory :public QObject, public LevelsFactory {

public:

   BonusCard* GetBonusTimeCard(){return new ExtraTimeBonusEz;}
   BadCard* GetBadTimeCard(){return new ReduseExtraTimeEz;}
   Timer* GetTimer(){return new TimerEz;}
};

class HardLevelFactory :public QObject, public LevelsFactory{
    Q_OBJECT

public:

 BonusCard* GetBonusTimeCard(){return new ExtraTimeBonusH;}
 BadCard* GetBadTimeCard(){return new ReduseExtraTimeH;}
 Timer* GetTimer(){return new TimerH;}

};
#endif // LEVELS_H
