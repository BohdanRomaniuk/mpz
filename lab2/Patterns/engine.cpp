#include "engine.h"
 Engine* Engine::Instanse=nullptr;
Engine::Engine()
{
connect(UpdataTimer, SIGNAL(timeout()), this, SLOT(UpdataTimerTimeout()));
}

void Engine::setTimeLabel(QLabel *value)
{
    TimeLabel = value;
    timer->setTimeLabel(TimeLabel);
}

Timer *Engine::getTimer() const
{
    return timer;
}
Engine* Engine::GetInstanse(){
    if(Instanse==nullptr)
      Instanse=new Engine;
  return Instanse;
}
void Engine::Start(LevelsFactory *Factory){
   Cards.push_back(Factory->GetBadTimeCard());
   Cards.push_back(Factory->GetBonusTimeCard());
   Cards.push_back(Cards[1]->Clone());
   Cards.push_back(new X4(Factory->GetBonusTimeCard()));
   timer=Factory->GetTimer();
   connect(timer, SIGNAL(timeout()), this, SLOT(TimerTimeout()));

}

void Engine::StartTimer()
{
    timer->StartTimer();
    UpdataTimer->start(1000);
}
void Engine::GenerareList(QListWidget *list){
    for (int i=0;i<Cards.size();++i) {
         list->addItem("Card№"+QString::number(i));
    }

}
Timer* Engine::UpdateTimer(){
    Timer *tmp=timer;
    timer=timer->Clone();
    delete tmp;
    return timer;
}

void Engine::DeleteCard(int number)
{
    Cards.remove(number);
}

void Engine::UpdataTimerTimeout(){
    TimeLabel->setText(QString::number(TimeLabel->text().toInt()-1));
}
void Engine::TimerTimeout(){};
