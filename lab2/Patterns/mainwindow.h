#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <engine.h>
#include "shopcontrollfacade.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    Engine *engine;
    ShopControllFacade* facade;
private slots:
    void on_ExitButton_clicked();

    void on_StartGameButton_clicked();

    void CreateEazyLevel();

    void CreateHardLevel();

    void on_ChoseButton_clicked();

    void on_BuyButton_click(int);

    void on_OpenShopButton_clicked();

    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;


};

#endif // MAINWINDOW_H
