#ifndef SHOP_H
#define SHOP_H
#include<QMutex>
#include<QLabel>
class Shop
{
private:
    int CountCoints=0;
    Shop();
     static Shop *Instanse;
     static QMutex *lock;
public:
    static Shop* GetInstanse();
    void AddCoins(int);
    void UpdataCoinsLabel(QLabel*);
};

#endif // SHOP_H
