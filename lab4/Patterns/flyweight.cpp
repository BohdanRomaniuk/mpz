#include "flyweight.h"

//QPixmap * Flyweight::BadCardPicture=new QPixmap("ddghfhf");
//QPixmap * Flyweight::BonusCardPicture=new QPixmap("dgfdg");
 Flyweight* Flyweight::Instanse=nullptr;
 QMutex* Flyweight::lock=new QMutex;
Flyweight::Flyweight()
{

}

Flyweight::Flyweight(QPixmap *Bad, QPixmap *Bonus)
{
    BadCardPicture=Bad;
    BonusCardPicture=Bonus;
}

Flyweight *Flyweight::GetInstanse()
{
    if (Instanse == nullptr)
       {
        lock->lock();
        if(Instanse==nullptr){
            Instanse=new Flyweight(new QPixmap("D:/Study/2/Term2/mpz/Lab7/Pictures/Badcard.png"),
                                   new QPixmap("D:/Study/2/Term2/mpz/Lab7/Pictures/BonusCard.png"));
        }
        lock->unlock();
    }
     return Instanse;
}

QPixmap *Flyweight::GetBadCardPicture()
{

    return BadCardPicture;
}

QPixmap *Flyweight::GetBonusCardPicture()
{

    return BonusCardPicture;
}
