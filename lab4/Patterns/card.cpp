#include "card.h"
#include "engine.h"

void ExtraTimeBonusEz::GetBonus(Engine *Eng)
{
  Eng->UpdateTimer()->setDurationLevel(TimeBonusEzLevel);
  Eng->StartTimer();
}

void ExtraTimeBonusH::GetBonus(Engine *Eng)
{
    Eng->UpdateTimer()->setDurationLevel(TimeBonusHLevel);
    Eng->StartTimer();
}

void ReduseExtraTimeEz::GetBadCard(Engine *Eng)
{

    Eng->UpdateTimer()->setDurationLevel(TimeBadEzLevel);
    Eng->StartTimer();
}

void ReduseExtraTimeH::GetBadCard(Engine *Eng)
{
    Eng->UpdateTimer()->setDurationLevel(TimeBadHLevel);
    Eng->StartTimer();
}

Card *Card::ShowPicture(QLabel *label)
{
    label->setPixmap(*CardPicture);
    return this;
}

void CardDecorator::SetTime(int mul)
{
   TimeBonusEzLevel*=mul;
   TimeBonusHLevel*=mul;
   TimeBadEzLevel*=mul;
   TimeBadHLevel*=mul;
}

void CardDecorator::BackTime(int div)
{
 TimeBadHLevel/=div;
 TimeBadEzLevel/=div;
 TimeBonusHLevel/=div;
 TimeBonusEzLevel/=div;
}
