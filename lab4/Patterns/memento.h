#ifndef MEMENTO_H
#define MEMENTO_H
#include"QString"
#include"QFile"
#include"QDataStream"
class Shop;
class Memento
{
    QString FileName;
public:
    Memento(QString savename);
    void Load();
};

#endif // MEMENTO_H
