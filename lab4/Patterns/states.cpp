#include "states.h"
#include"audioplayer.h"
void States::setContext(AudioPlayer *value)
{
    context = value;
}

void States::clickNext()
{
    context->nextSong();
}

void States::clickPrevious()
{
    context->previousSong();
}

States::States()
{

}

States::States(AudioPlayer *_context)
{
    this->context=_context;
}

void PlayingState::clickLock()
{
    context->ChangeState(new ReadyState(context));
    context->pausePlayback();
}

void PlayingState::clickPlay()
{

}

void ReadyState::clickLock()
{

}

void ReadyState::clickPlay()
{
    context->ChangeState(new PlayingState(context));
    context->startPlayback();
}
