#ifndef OBSERVER_H
#define OBSERVER_H
class ShopEventManager;
class Shop;

class BonusListener
{

public:
    BonusListener();
    virtual void Update(ShopEventManager*)=0;
};

class SmallBonusListener:public BonusListener{
    void Update(ShopEventManager*);
};
class BigBonusListener:public BonusListener{
    void Update(ShopEventManager*);
};

#endif // OBSERVER_H
