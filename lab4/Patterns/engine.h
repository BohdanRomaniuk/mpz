#ifndef ENGINE_H
#define ENGINE_H

#include"levels.h"
#include"QListWidget"
#include"QListWidgetItem"
#include"comand.h"

class Engine :public QObject
{
Q_OBJECT
    Comand *comand;
private:
    Engine();
     static Engine *Instanse;
     QVector<Card*> Cards;
     QLabel *TimeLabel;
     Timer *timer;
     QTimer *UpdataTimer=new QTimer;


public:
    static Engine* GetInstanse();
    void Start(LevelsFactory*);
    void StartTimer();
    void GenerareList(QListWidget *);
    Timer* UpdateTimer();
    void DeleteCard(int);
    Card* GetCard(int number){
        return Cards[number];
    }

    Timer *getTimer() const;
    void setTimeLabel(QLabel *value);


    void setComand(Comand *value);
    bool executeCommand();

private slots:
    void TimerTimeout();
    void UpdataTimerTimeout();
};
//private s
#endif // ENGINE_H
