#ifndef SERVER_H
#define SERVER_H
#include"QString"
#include"QMap"

class Server
{
    QMap<QString, QString> Users;
public:
    Server();
    bool FindUser(QString,QString);
    bool AddUser(QString,QString);
};

#endif // SERVER_H
