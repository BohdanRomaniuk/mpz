#ifndef TIMER_H
#define TIMER_H
#include "QTimer"
#include <QLabel>

class Timer: public QTimer
{
protected:
   int durationLevel=1000;
   QLabel  *TimeLabel;

public:
    Timer();
    virtual void StartTimer()=0;

    void setTimeLabel(QLabel *value);
    void setDurationLevel(int value);
    virtual Timer* Clone()=0;
};


class TimerEz :public Timer{

public:
    TimerEz(){ durationLevel=1000*60*8;}
    TimerEz(TimerEz* Copy){this->TimeLabel=Copy->TimeLabel; this->durationLevel=Copy->durationLevel; }
    Timer* Clone(){return new TimerEz(this);}
    void StartTimer();
};

class TimerH :public Timer{

public:
    TimerH(){ durationLevel=1000*60*3;}
    TimerH(TimerH* Copy){this->TimeLabel=Copy->TimeLabel; this->durationLevel=Copy->durationLevel; }
    Timer* Clone(){return new TimerH(this);}
    void StartTimer();
};
#endif // TIMER_H
