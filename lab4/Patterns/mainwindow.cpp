#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSignalMapper>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QSignalMapper* signalMapper = new QSignalMapper (this) ;
    engine=Engine::GetInstanse();
    facade=new ShopControllFacade;
    audioplayer=new AudioPlayer(new ReadyState);
    receiver= new Server;
    sem=Shop::GetInstanse()->getShopEventManager();

    connect (ui->Buy20, SIGNAL(clicked()), signalMapper, SLOT(map())) ;
    connect (ui->Buy30, SIGNAL(clicked()), signalMapper, SLOT(map())) ;
    connect (ui->Buy40, SIGNAL(clicked()), signalMapper, SLOT(map())) ;

    signalMapper -> setMapping (ui->Buy20, 20) ;
    signalMapper -> setMapping (ui->Buy30, 30) ;
    signalMapper -> setMapping (ui->Buy40, 40) ;

    connect(ui->EazyLevel,SIGNAL(clicked()),this,SLOT(CreateEazyLevel()));
    connect(ui->HardLevel,SIGNAL(clicked()),this,SLOT(CreateHardLevel()));

    connect(signalMapper,SIGNAL(mapped(int)),this,SLOT(on_BuyButton_click(int)));

    ui->Back->setVisible(false);
    ui->ExitAcc->setVisible(false);

    sem->AttachObserver(new SmallBonusListener);
    sem->AttachObserver(new BigBonusListener);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::CreateEazyLevel()
{
    ui->stackedWidget->setCurrentIndex(ui->stackedWidget->currentIndex()-1);
    LevelsFactory* Factory=new EazyLevelFactory;
    engine=Engine::GetInstanse();
    engine->Start(Factory);
    engine->GenerareList(ui->listWidget);
    engine->setTimeLabel(ui->Time);
    engine->StartTimer();
}

void MainWindow::on_ExitButton_clicked()
{
    exit(0);
}

void MainWindow::on_StartGameButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(ui->stackedWidget->currentIndex()-1);

}

void MainWindow::CreateHardLevel()
{
    ui->stackedWidget->setCurrentIndex(ui->stackedWidget->currentIndex()-1);
    LevelsFactory* Factory=new HardLevelFactory;
    engine=Engine::GetInstanse();
    engine->Start(Factory);
    engine->GenerareList(ui->listWidget);
    engine->setTimeLabel(ui->Time);
    engine->StartTimer();
}

void MainWindow::on_ChoseButton_clicked()
{
    delete  engine->GetCard(ui->listWidget->currentRow())->FlipCard(engine)->ShowPicture(ui->LastCardPicture);
    engine->DeleteCard(ui->listWidget->currentRow());
    ui->listWidget->takeItem(ui->listWidget->currentRow());

}

void MainWindow::on_BuyButton_click(int coins)
{
  memento=Shop::GetInstanse()->Save(FilePath);
  facade->BuyCoins(coins,ui->CoinsCount);
  ui->Back->setVisible(true);
}

void MainWindow::on_OpenShopButton_clicked()
{
     ui->stackedWidget->setCurrentIndex(ui->stackedWidget->currentIndex()-3);
}

void MainWindow::on_pushButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(ui->stackedWidget->currentIndex()+3);
    Shop::GetInstanse()->getShopEventManager()->setBoughtCoins(0);
}

void MainWindow::on_Back_clicked()
{
    Shop::GetInstanse()->Load(memento);
    Shop::GetInstanse()->UpdataCoinsLabel(ui->CoinsCount);
    //Shop::GetInstanse()->getShopEventManager()->;
    ui->Back->setVisible(false);
}

void MainWindow::on_PlayAudio_clicked()
{
    audioplayer->clickPlay();
}

void MainWindow::on_PauseAudio_clicked()
{
    audioplayer->clickLock();
}

void MainWindow::on_NextAudio_clicked()
{
    audioplayer->clickNext();
}

void MainWindow::on_PrevAudio_clicked()
{
    audioplayer->clickPrevious();
}

void MainWindow::on_pushButton_2_clicked()
{
    engine->setComand(new AuthorizationComand(ui->LoginIn->text(),ui->PassIn->text(),receiver));
    if(engine->executeCommand()){
        ui->Welcome->setText("Welcome "+ui->LoginIn->text());
        ui->RegistrationBox->setVisible(false);
        ui->ExitAcc->setVisible(true);
    }
}

void MainWindow::on_pushButton_3_clicked()
{
    engine->setComand(new RegisterComand(ui->LoginReg->text(),ui->PassReg->text(),receiver));
    engine->executeCommand();
    ui->Welcome->setText("Welcome "+ui->LoginReg->text());
    ui->RegistrationBox->setVisible(false);
    ui->ExitAcc->setVisible(true);
}

void MainWindow::on_ExitAcc_clicked()
{
    ui->Welcome->setText("");
    ui->RegistrationBox->setVisible(true);
    ui->ExitAcc->setVisible(false);
}
