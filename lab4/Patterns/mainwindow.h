#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <engine.h>
#include "shopcontrollfacade.h"
#include "audioplayer.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    Engine *engine;
    ShopControllFacade* facade;
    Memento *memento=nullptr;
    AudioPlayer *audioplayer;
    Server *receiver;
    QString FilePath="D:/Study/2/Term2/mpz/Lab7/Save.txt";
    ShopEventManager* sem;
private slots:
    void on_ExitButton_clicked();

    void on_StartGameButton_clicked();

    void CreateEazyLevel();

    void CreateHardLevel();

    void on_ChoseButton_clicked();

    void on_BuyButton_click(int);

    void on_OpenShopButton_clicked();

    void on_pushButton_clicked();

    void on_Back_clicked();

    void on_PlayAudio_clicked();

    void on_PauseAudio_clicked();

    void on_NextAudio_clicked();

    void on_PrevAudio_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_ExitAcc_clicked();

private:
    Ui::MainWindow *ui;


};

#endif // MAINWINDOW_H
