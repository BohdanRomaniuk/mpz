#include "shopeventmanager.h"

int ShopEventManager::getBoughtCoins() const
{
    return BoughtCoins;
}

void ShopEventManager::setBoughtCoins(int value)
{
    BoughtCoins = value;
}

bool ShopEventManager::getSmallBonusReceived() const
{
    return SmallBonusReceived;
}

void ShopEventManager::setSmallBonusReceived(bool value)
{
    SmallBonusReceived = value;
}

bool ShopEventManager::getBigBonusReceived() const
{
    return BigBonusReceived;
}

void ShopEventManager::setBigBonusReceived(bool value)
{
    BigBonusReceived = value;
}

ShopEventManager::ShopEventManager()
{

}

void ShopEventManager::AttachObserver(BonusListener *bl)
{
    Observers.push_back(bl);
}

void ShopEventManager::Notify()
{
    foreach(BonusListener *ob, Observers){
        ob->Update(this);
    }
}
